(specifications->manifest
 '("aalib"
   "alsa-lib"
   "appstream-glib"
   "atk"
   "autoconf"
   "automake"
   "babl-with-introspection"
   "bzip2"
   "cairo"
   "desktop-file-utils"
   "fontconfig"
   "freetype"
   "gcc-toolchain@10"
   "gdk-pixbuf+svg"
   "gegl-with-introspection"
   "gettext"
   "gexiv2"
   "ghostscript"
   "git"
   "gjs"
   "glib"
   "glib-networking"
   "gobject-introspection"
   "gtk+"
   "gtk+:bin"
   "gtk-doc"
   "harfbuzz"
   "intltool"
   "iso-codes"
   "json-glib"
   "lcms"
   "libarchive"
   "libgudev"
   "libheif"
   "libjpeg"
   "libmng"
   "libmypaint"
   "libpng"
   "librsvg"
   "libtiff"
   "libtool"
   "libunwind"
   "libwebp"
   "libxmu"
   "libxpm"
   "libxslt"
   "libxt"
   "luajit"
   "m4"
   "make"
   "meson@0.55"
   "mypaint-brushes@1.3"
   "ninja"
   "openexr"
   "openjpeg"
   "pkg-config"
   "poppler"
   "poppler-data"
   "python"
   "python-pygobject"
   "vala"
   "webkitgtk"
   "xz"))
