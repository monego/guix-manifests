(define-module (pkg gimp)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages build-tools)
  #:use-module (gnu packages fonts)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gimp)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pdf)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages web))

(define-public babl-with-introspection
  (package
    (inherit babl)
    (name "babl-with-introspection")
    (arguments
     `(#:meson ,meson-0.55
       #:configure-flags
       (list "-Denable-gir=true"
             "-Denable-vapi=true")))
    (native-inputs
     `(("gobject-introspection" ,gobject-introspection)
       ("pkg-config" ,pkg-config)
       ("vala" ,vala)))))

(define-public gegl-with-introspection
  (package
    (inherit gegl)
    (name "gegl-with-introspection")
    (arguments
     `(#:configure-flags
       (list "-Dintrospection=true")
       ,@(package-arguments gegl)))
    ;; These are propagated to satisfy 'gegl-0.4.pc'.
    (propagated-inputs
     `(("babl" ,babl-with-introspection)
       ("glib" ,glib)
       ("json-glib" ,json-glib)))
    (inputs
     ;; All the inputs are optional.
     `(("cairo" ,cairo)
       ("gdk-pixbuf" ,gdk-pixbuf)
       ("gexiv2" ,gexiv2)
       ("jasper" ,jasper)
       ("pango" ,pango)
       ("libpng" ,libpng)
       ("libjpeg" ,libjpeg-turbo)
       ("libnsgif" ,libnsgif)
       ("libraw" ,libraw)
       ("librsvg" ,librsvg)
       ("libspiro" ,libspiro)
       ("libtiff" ,libtiff)
       ("libwebp" ,libwebp)
       ("maxflow" ,maxflow)
       ("openexr" ,openexr)
       ("poppler" ,poppler)
       ("sdl2" ,sdl2)))
    (native-inputs
     `(("pkg-config" ,pkg-config)
       ("glib" ,glib "bin")             ; for gtester
       ("gobject-introspection" ,gobject-introspection)
       ("intltool" ,intltool)
       ("vala" ,vala)))))
